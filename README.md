# Team 4 - API Coconutplay

Eddy ABADA - ABAE17039808

### Installation

Exécuter la commande suivante :

```
npm install
```

### Lancer l'application

Pour lancer l'application, lancer la commande suivante :

```
npm run start
```

### Listes des comptes utilisateurs créés au lancement de l'API (une fois)

```
username: eliop
email: eliop@gmail.com
password: aciman

member
```
```
username: mika99
email: mika99@gmail.com
password: savagelord

admin
```

### NB

Ce qui est implémenté :
    - Lors du lancement de l'application, deux comptes utilisateurs sont créés
    - Il est possible de créer un compte utilisateur avec un ou plusieurs rôles (admin, membre)
    - Les utilisateurs sont sauvegardés dans la base de données
    - Il est possible de se connecter en utilisant au choix son username ou son email

Problèmes rencontrés :
    Au moment où j'ai essayé de faire le endpoint '/profile/me', je me suis heurté au problème suivant :
        Il n'est pas possible de récupérer le token dans req.headers['access-token'], pourtant un token unique est bien généré lors de la connection d'un utilisateur