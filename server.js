import express from "express";
import bodyParser from "body-parser";
import cors from 'cors';
import {getGames, putGame, putComment} from "./games.js";
import { user } from "./models/index.js";

const db = require('./models');
const dbConfig = require('./config/db.config');
const Role = db.role;
const createUsers = require('./middleware/createUsers.js');

db.mongoose
    .connect(`mongodb://${dbConfig._HOST}:${dbConfig._PORT}/${dbConfig._DB}`, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
    .then(() => {
        console.log('Successfully connect to MongoDB');
        initial();
    })
    .catch(err => {
        console.error('Connection error', err);
        process.exit();
    })

function initial(){
    Role.estimatedDocumentCount((err, count) => {
        if(!err && count === 0){
            new Role({
                name: 'admin'
            }).save(err => {
                if(err) console.log('error', err);
                console.log("added 'admin' to roles collection");
            });

            new Role({
                name: 'member'
            }).save(err => {
                if(err) console.log('error', err);
                console.log("added 'member' to roles collection");
            });
        };
    });
    createUsers();
};

const app = express();
const _PORT = 3000;

var games = getGames();

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.get('/games', (req, res) => {
    res.send(games);
});

app.get('/game/:id', (req, res) => {
    var index_of_game = 0;
    for(var i = 0; i < games.length; ++i){
        if(games[i].id == req.params.id){
            index_of_game = i;
            break;
        } 
    }
    res.send(games[index_of_game]);
});

app.put('/game', (req, res) => {
    putGame(req, res, games);
});

app.get('/game/:id/comments', (req, res) => {
    var index_of_game = 0;
    for(var i = 0; i < games.length; ++i){
        if(games[i].id == req.params.id){
            index_of_game = i;
            break;
        } 
    }
    res.send(JSON.parse(JSON.stringify(games[index_of_game].comments)));
});

app.put('/game/:id/comment', (req, res) => {
    putComment(req, res, games);
});

app.delete('/game/:id/comment/:id1', (req, res) => {
    var index_of_game = 0;
    var index_of_comment = req.params.id1 - 1;
    for(var i = 0; i < games.length; ++i){
        if(games[i].id == req.params.id){
            index_of_game = i;
            break;
        } 
    }
    games[index_of_game].comments.splice(index_of_comment, 1);
    res.send("deletion completed");
});

app.get('/', (req, res) => {
    res.send("Pitayaplay API");
});

// user endpoints

require('./routes/auth.routes')(app);

app.use((req, res) => {
    res.status(404).send({
        url: req.originalUrl + ' not found'
    });
});

app.listen(_PORT, () => {
    console.log("server running on port " + _PORT);
});