const db = require('../models');
const User = db.user;
const Role = db.role;
var bcrypt = require('bcryptjs');

const users = [
    {
        "username": "eliop",
        "email": "eliop@gmail.com",
        "password": "aciman",
        "name": "Perlman",
        "firstname": "Elio",
        "birthdate": "1996-06-06",
        "roles":[
            "member"
        ]
    },
    {
        "username": "mika99",
        "email": "mika99@gmail.com",
        "password": "savagelord",
        "name": "Savage",
        "firstname": "Mikaël",
        "birthdate": "1999-05-28",
        "roles":[
            "admin", "member"
        ]
    }
];

const createUsers = () => {
    users.forEach(function(user){
        User.findOne({
            username: user.username
        })
        .exec((err, userFound) => {
            if(err) {
                console.log(err);
                return;
            }
            if(userFound) return;

            const newUser = new User({
                username: user.username,
                email: user.email,
                password: bcrypt.hashSync(user.password, 8),
                name: user.name, 
                firstname: user.firstname,
                birthdate: user.date
            });

            newUser.save((err, user) => {
                if(err){
                    console.log(err);
                    return;
                };
        
                if(user.roles.length > 0){
                    Role.find(
                        {
                            name: { $in: req.body.roles }
                        },
                        (err, roles) => {
                            if(err){
                                console.log(err);
                                return;
                            };
                            
                            newUser.roles = roles.map(role => role._id);
                            newUser.save(err => {
                                if(err){
                                    console.log(err);
                                    return;
                                };
        
                                console.log('User registred');
                            });
                        }
                    );
                } else {
                    Role.findOne({ name: 'member' }, (err, role) => {
                        if(err){
                            console.log(err);
                            return;
                        };
        
                        newUser.roles = [role._id];
                        newUser.save(err => {
                            if(err){
                                console.log(err);
                                return;
                            };
        
                            console.log('User registred');
                        });
                    });
                };
            });
        });
    });
};

module.exports = createUsers;