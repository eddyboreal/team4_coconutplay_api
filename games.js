const article = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ante metus dictum at tempor commodo ullamcorper a lacus vestibulum. Viverra orci sagittis eu volutpat. Magna ac placerat vestibulum lectus. \n Sit amet consectetur adipiscing elit pellentesque habitant morbi tristique senectus. Molestie ac feugiat sed lectus vestibulum mattis ullamcorper velit. Faucibus interdum posuere lorem ipsum dolor sit amet consectetur adipiscing. Dui ut ornare lectus sit amet est placerat in egestas. \nQuis varius quam quisque id diam vel quam. Egestas erat imperdiet sed euismod. Condimentum lacinia quis vel eros donec. Eget mi proin sed libero enim sed faucibus turpis. Justo laoreet sit amet cursus sit amet. Mattis molestie a iaculis at. Phasellus egestas tellus rutrum tellus pellentesque eu. \nUt porttitor leo a diam sollicitudin tempor id eu nisl. Blandit volutpat maecenas volutpat blandit. Quisque sagittis purus sit amet volutpat consequat. Proin fermentum leo vel orci porta non pulvinar. Lorem sed risus ultricies tristique nulla aliquet enim tortor. Placerat vestibulum lectus mauris ultrices eros in. Id venenatis a condimentum vitae sapien pellentesque habitant morbi tristique. Vitae justo eget magna fermentum. Libero nunc consequat interdum varius sit amet. Vestibulum mattis ullamcorper velit sed ullamcorper. Risus feugiat in ante metus dictum at tempor commodo ullamcorper. Urna nunc id cursus metus aliquam eleifend mi in. Aliquam vestibulum morbi blandit cursus. Vitae ultricies leo integer malesuada nunc. Rutrum tellus pellentesque eu tincidunt tortor. Neque aliquam vestibulum morbi blandit cursus risus at ultrices. Vehicula ipsum a arcu cursus. Arcu cursus euismod quis viverra nibh. Arcu odio ut sem nulla pharetra diam sit amet. Volutpat consequat mauris nunc congue. Ultrices dui sapien eget mi proin. Ac tortor vitae purus faucibus. Ornare massa eget egestas purus viverra. Ultricies mi quis hendrerit dolor magna eget. Urna condimentum mattis pellentesque id nibh tortor id aliquet. Enim ut sem viverra aliquet. Sed risus pretium quam vulputate dignissim suspendisse in. Non arcu risus quis varius quam quisque id diam. Volutpat ac tincidunt vitae semper quis lectus nulla at volutpat. Ut sem nulla pharetra diam sit. Ipsum dolor sit amet consectetur adipiscing elit pellentesque habitant morbi. Diam quam nulla porttitor massa id. Mattis ullamcorper velit sed ullamcorper morbi tincidunt ornare massa. Tincidunt nunc pulvinar sapien et ligula ullamcorper malesuada. Gravida cum sociis natoque penatibus et. Turpis in eu mi bibendum neque egestas congue quisque. Morbi blandit cursus risus at ultrices mi tempus imperdiet nulla. Interdum posuere lorem ipsum dolor. Massa vitae tortor condimentum lacinia quis vel eros. Viverra vitae congue eu consequat ac. Donec enim diam vulputate ut pharetra sit amet. Integer enim neque volutpat ac tincidunt vitae semper quis. Amet dictum sit amet justo. Mattis nunc sed blandit libero volutpat sed cras ornare arcu. Sed enim ut sem viverra aliquet eget sit amet tellus. Posuere lorem ipsum dolor sit. Mi bibendum neque egestas congue. Egestas fringilla phasellus faucibus scelerisque eleifend. Nunc aliquet bibendum enim facilisis gravida neque convallis a cras. Proin nibh nisl condimentum id venenatis a condimentum vitae sapien. Mauris in aliquam sem fringilla ut. Scelerisque viverra mauris in aliquam sem fringilla. Enim facilisis gravida neque convallis a cras semper auctor neque. Elit ullamcorper dignissim cras tincidunt. Placerat in egestas erat imperdiet. Nunc pulvinar sapien et ligula ullamcorper malesuada proin libero nunc. Aenean pharetra magna ac placerat vestibulum lectus mauris. Et malesuada fames ac turpis egestas sed tempus urna et. Pellentesque sit amet porttitor eget. At tellus at urna condimentum. Urna neque viverra justo nec ultrices dui sapien eget. Sollicitudin tempor id eu nisl nunc. Amet volutpat consequat mauris nunc.";

var games = [
    {
        id: 1,
        title: "The Witcher 3 Wild Hunt - What about Geralt of Rivia ?",
        text: article,
        author: "Elio Perlman",
        image_url: "TW3.jpg",
        date: "05/03/2020",
        name: "The Witcher 3",
        category: "RPG",
        note: "16.5/20",
        comments: [
            {
                id: 1,
                name: "Oliver",
                mail: "oliver@gmail.com",
                text: "This is a comment !"
            }
        ]
    },
    {
        id: 2,
        title: "God of War - What is new about the great Kratos in GoW?     ",
        text: article,
        author: "Corey Barlog",
        image_url: "GOW.jpg",
        date: "05/03/2020",
        name: "God of War",
        category: "RPG",
        note: "19/20",
        comments: [
            {
                id: 1,
                name: "Oliver",
                mail: "oliver@gmail.com",
                text: "This is a comment !"
            }
        ]
    },
    {
        id: 3,
        title: "Call of Duty - The Test",
        text: article,
        author: "Bong Joon-Oh",
        image_url: "CallOfDuty.jpg",
        date: "05/03/2020",
        name: "Call of Duty",
        category: "FPS",
        note: "13/20",
        comments: [
            {
                id: 1,
                name: "Oliver",
                mail: "oliver@gmail.com",
                text: "This is a comment !"
            }
        ]
    },
    {
        id: 4,
        title: "Singularity - Discover the new franchise : The test",
        text: article,
        author: "Kim Il-Sung",
        image_url: "singularity.jpg",
        date: "05/03/2020",
        name: "Singularity",
        category: "FPS",
        note: "19/20",
        comments: [
            {
                id: 1,
                name: "Oliver",
                mail: "oliver@gmail.com",
                text: "This is a comment !"
            }
        ]
    },
    {
        id: 5,
        title: "Bioshock - The Test",
        text: article,
        author: "Jodie Holmes",
        image_url: "bioshock.jpg",
        date: "05/03/2020",
        name: "Bioshock",
        category: "FPS",
        note: "15/20",
        comments: [
            {
                id: 1,
                name: "Oliver",
                mail: "oliver@gmail.com",
                text: "This is a comment !"
            }
        ]
    },
    {
        id: 6,
        title: "Metro - The Test",
        text: article,
        author: "Max Caufield",
        image_url: "metro.jpg",
        date: "05/03/2020",
        name: "Metro",
        category: "FPS",
        note: "12.5/20",
        comments: [
            {
                id: 1,
                name: "Oliver",
                mail: "oliver@gmail.com",
                text: "This is a comment !"
            }
        ]
    },
    {
        id: 7,
        title: "World of Warcraft - Get ready for a new extension : The test",
        text: article,
        author: "Chloe Price",
        image_url: "wow.jpg",
        date: "05/03/2020",
        name: "World of Warcraft",
        category: "MMO",
        note: "17.5/20",
        comments: [
            {
                id: 1,
                name: "Oliver",
                mail: "oliver@gmail.com",
                text: "This is a comment !"
            }
        ]
    },
    {
        id: 8,
        title: "GuildWars 2 - The Test",
        text: article,
        author: "Mikaël Fructuoso",
        image_url: "guildwars2.jpg",
        date: "05/03/2020",
        name: "GuildWars 2",
        category: "MMO",
        note: "17/20",
        comments: [
            {
                id: 1,
                name: "Oliver",
                mail: "oliver@gmail.com",
                text: "This is a comment !"
            }
        ]
    },
    {
        id: 9,
        title: "Aion - The Test",
        text: article,
        author: "Charles Dawkins",
        image_url: "aion.jpg",
        date: "05/03/2020",
        name: "Aion",
        category: "MMO",
        note: "8/20",
        comments: [
            {
                id: 1,
                name: "Oliver",
                mail: "oliver@gmail.com",
                text: "This is a comment !"
            }
        ]
    },
    {
        id: 10,
        title: "Starcraft - A game that will test your skills : The test",
        text: article,
        author: "David Solans",
        image_url: "starcraft.jpg",
        date: "05/03/2020",
        name: "Starcraft",
        category: "RTS",
        note: "11/20",
        comments: [
            {
                id: 1,
                name: "Oliver",
                mail: "oliver@gmail.com",
                text: "This is a comment !"
            }
        ]
    },
    {
        id: 11,
        title: "Dawn of War : Warhammer 40.000 - The Test",
        text: article,
        author: "Bruno Bergeron",
        image_url: "dawnofwar.jpg",
        date: "05/03/2020",
        name: "Dawn of War : Warhammer 40.000",
        category: "RTS",
        note: "14/20",
        comments: [
            {
                id: 1,
                name: "Oliver",
                mail: "oliver@gmail.com",
                text: "This is a comment !"
            }
        ]
    },
    {
        id: 12,
        title: "Age of Empire - The Test",
        text: article,
        author: "Tatara Fujita",
        image_url: "ageofempire.jpg",
        date: "05/03/2020",
        name: "Age of Empire",
        category: "RTS",
        note: "12.5/20",
        comments: [
            {
                id: 1,
                name: "Elio",
                mail: "elio@gmail.com",
                text: "This is a comment !"
            },
            {
                id: 2,
                name: "Mikaël",
                mail: "mika@gmail.com",
                text: "This is another comment !"
            }
        ]
    }
];

export function getGames(){
    return games;
};

export function putGame(req, res, games){
    const id = games.length + 1;
    const title = req.body.title;
    const text = req.body.text;
    const author = req.body.author;
    const image_url = "new-test.png";
    const date = req.body.date;
    const name = req.body.name;
    const category = req.body.category;
    const note = req.body.note;

    var new_game = {
        id: id,
        title: title,
        text: text,
        author: author,
        image_url: image_url,
        date: date,
        name: name,
        category: category,
        note: note,
        comments: []
    };
    games.push(new_game)
    res.send("new game was added");
};

export function putComment(req, res, games){
    const id = games[req.params.id - 1].comments.length + 1;
    const mail = req.body.mail;
    const text = req.body.text;
    const name = req.body.name;

    var new_comment = {
        id: id,
        mail: mail,
        name: name,
        text: text
    };
    games[req.params.id - 1].comments.push(new_comment);
    res.send(req.body);
};