exports.allAccess = (req, res) => {
    res.status(200).send('Public content');
};

exports.memberAccess = (req, res) => {
    res.status(200).send('Member content');
};

exports.adminAccess = (req, res) => {
    res.status(200).send('Admin content');
};

